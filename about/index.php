<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title>About Us - Union System Global</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="76x76" href="../images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../images/favicon/favicon-16x16.png">
    <link rel="manifest" href="../images/favicon/site.webmanifest">
    <link rel="mask-icon" href="../images/favicon/safari-pinned-tab.svg" color="#8e097a">
    <link rel="shortcut icon" href="../images/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="../images/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900,300italic">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">

    <style>
    .parallax-container:after{
      content:"";
      display:block;
      position:absolute;
      top:0;
      bottom:0;
      left:0;
      right:0;
      background:rgba(0,0,0,0.2);
    }
    </style>

		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body>
    <!-- Page-->
    <div class="page">
      <div class="page-loader">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
          <p class="loader-header">Loading</p>
        </div>
      </div>
      <!-- Page Header-->
      <header class="page-head">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-contrast" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-static" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-lg-stick-up-clone="true" data-md-stick-up-offset="114px" data-lg-stick-up-offset="114px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
            <div class="rd-navbar-aside-outer novi-background">
              <div class="rd-navbar-aside-inner">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="../"><img src="../images/logo.png" alt="" width="158" height="40"/></a>
                </div>
                <div class="rd-navbar-aside">
                  <div class="rd-navbar-aside-toggle" data-rd-navbar-toggle=".rd-navbar-aside"><span></span></div>
                  <div class="rd-navbar-aside-content">
                    <ul class="block-wrap-list">
                      <li class="block-wrap">
                        <div class="unit unit-sm-horizontal unit-align-center unit-middle unit-spacing-sm">
                          <div class="unit-left">
                            <div class="icon novi-icon fl-bigmug-line-big104"></div>
                          </div>
                          <div class="unit-body">
                            <address class="contact-info"><a href="https://www.google.com.gh/maps/place/UNION+SYSTEMS+GLOBAL+GH.LTD/@5.5491945,-0.2356864,17z/data=!3m1!4b1!4m5!3m4!1s0xfdf974f85f3e69f:0x25e5aa81924b253e!8m2!3d5.5491892!4d-0.2334977"><span class="text-bold text-gray-base">No 4, Jungle Lane</span><span>Lartebiokorshie, Accra, Ghana</span></a></address>
                          </div>
                        </div>
                      </li>
                      <li class="block-wrap">
                        <div class="unit unit-sm-horizontal unit-align-center unit-middle unit-spacing-sm">
                          <div class="unit-left">
                            <div class="icon novi-icon fl-bigmug-line-phone351"></div>
                          </div>
                          <div class="unit-body">
                            <address class="contact-info"><span class="text-bold"><a class="text-gray-base" href="tel:#">+233 26 685 2291</a></span><span><a href="mailto:#">info@unionsg.com</a></span></address>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="rd-navbar-inner rd-navbar-nav-dark novi-background">
              <div class="rd-navbar-nav-wrap">
                <div class="rd-navbar-nav-wrap-inner">
                  <div class="elements-group-1"><span class="small text-italic text-white">Follow Us:</span>
                    <ul class="list-inline">
                      <li><a class="icon icon-xxs icon-circle icon-lynch icon-fiord-filled fa-facebook novi-icon" href="#"></a></li>
                      <li><a class="icon icon-xxs icon-circle icon-lynch icon-fiord-filled fa-twitter novi-icon" href="#"></a></li>
                      <li><a class="icon icon-xxs icon-circle icon-lynch icon-fiord-filled fa-google-plus novi-icon" href="#"></a></li>
                    </ul>
                  </div>
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li><a href="../">Home</a></li>

                    <li>
                      <a href="#">Segments</a>
                      <ul class="rd-navbar-dropdown">
                        <li><a href="../segments/banks/">Banks</a></li>
                        <li><a href="../segments/microfinance/">Microfinance</a></li>
                        <li><a href="../segments/credit-unions/">Credit Unions</a></li>
                        <li><a href="../segments/finance-houses/">Finance Houses</a></li>
                      </ul>
                    </li>
                    
                    <li><a href="#">Services</a>
                      <ul class="rd-navbar-dropdown">
                        <li><a href="../services/implementations/">Implementations</a></li>
                        <li><a href="../services/dcs-drm/">Data Centre Setup</a></li>
                        <li><a href="../services/dcs-drm/">Disaster Recovery Management</a></li>
                        <li><a href="../services/systems-integration/">Systems Integration</a></li>
                        <li><a href="../services/business-intelligence/">Business Intelligence</a></li>
                        <li><a href="../services/architecture-design/">Architecture Design</a></li>
                        <li><a href="../services/training/">Training</a></li>
                      </ul>
                    </li>
                    <li><a href="#">Solutions</a>
                    <ul class="rd-navbar-dropdown">
                        <li><a href="../solutions/core-banking/">Core Banking</a></li>
                        <li><a href="../solutions/non-core-banking/">Non Core Banking</a></li>
                        <li><a href="../solutions/channels/">Alternate Channels</a></li>
                      </ul>
                    </li>
                    <li class="active"><a href="">About</a>
                      <ul class="rd-navbar-dropdown">
                        <li><a href="./">About Us</a></li>
                        <li><a href="./clients/">Our Clients</a></li>
                        <li><a href="./careers/">Careers</a></li>
                        <li><a href="./team/">Our Team</a></li>
                        <li><a href="./alliances">Alliances</a></li>
                      </ul>
                    </li>
                    <li><a href="../contacts/">Contact Us</a></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Page Content-->
      <section class="parallax-container bg-river-bed" data-parallax-img="../images/about.jpg">
        <div class="parallax-content section-45 section-sm-top-60 section-sm-bottom-68">
          <div class="shell text-center">
            <div class="range range-fix">
              <div class="cell-xs-12">
                <h2>About Us</h2>
                <hr class="hr-block hr-white"/>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="border-bottom border-bottom-gray-lighter novi-background bg-cover">
        <div class="shell">
          <div class="range range-fix">
            <div class="cell-xs-12">
              <ul class="breadcrumbs-custom">
                <li><a href="../../">Home</a></li>
                <li class="active">About Us</li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <!-- About us-->
      <section class="section-top-55 section-bottom-60 novi-background bg-cover">
        <div class="shell">
          <div class="range range-fix">
            <div class="cell-xs-12 cell-md-8 cell-lg-9 content-inner">
              <div class="section-inner section-30">
                <div class="inset-md-right-15 shift-up-sm-10">
                  <img src="../images/typography-1-1169x610.jpg" alt="" width="1169" height="610"/>
                  <h3 class="offset-top-30">Who We Are</h3>
                  <p class="text-style-1 offset-top-22">
                  USG is a Industry leader in Financial Software development and sales, Datacenters/Disaster-Recovery building, consulting and outsourcing. 
                  </p>
                  <p class="text-style-1 offset-top-22">
                  We help our clients, to stay a ahead of emerging business trends and outperform the competition.  
                  </p>
                  <p class="text-style-1 offset-top-22">
                  Our experience gives our clients a distinct advantage. In addition to helping them manage their business, we can power their transformation to a smarter organization as well. This allows them to focus on their core business priorities.  
                  </p>
                  <p class="text-style-1 offset-top-22">
                  Established in 1998, USG has many products developed and implemented successfully to its credit including its award winning Core Banking System X.100, Lending System, Accounting and the popular Document/Image management systems.  
                  </p>
                  <p class="text-style-1 offset-top-22">
                  X.100 Core Banking Software is a comprehensive, integrated-yet-modular core banking solution that caters for all the needs of a modern financial institution and its multiple business segments. it address the needs of Retail, Corporate, universal banks and Others via: core banking solution, e-banking, mobile banking, CRM, Payments, treasury, Loan origination and liquidity management.   
                  </p>
                  <p class="text-style-1 offset-top-22">
                  X.100 gives you the unique advantage of implementing a single Banking Software seamlessly across the organization.X.100 has been ranked as one of top Core Banking System of recent times due to its flexibility and ease of use coupled with all the security systems implemented. It has helped banks to reduce costs and in turn improve the bottom-line and stakeholder rewards.It derives its rich functionality from our extensive experience and expertise as a leading provider of Browser Based Online banking Software solutions. 
                  </p>
                  <p class="text-style-1 offset-top-22">
                  The flexible, functional architecture of X.100 enables it to be deployed across a variety of scenarios depending on the financial institution's business model. It is built to run a range of functions: Retail and Corporate Banking, Treasury, Investment and Multiple Delivery Channel Support. It also supports high-end Business Intelligence, Internet banking, Mobile banking etc. Its contemporary architecture provides the capability to scale your business operations. It offer to multiple delivery channels viz: Internet Banking, RTGS, EFT Switch, SWIFT, CTS, ACH, CRM, Mobile Banking
                  </p>
                  <p class="text-style-1 offset-top-22">
                  Union Systems has enriched Quality processes & systems in areas of software development & deployment, information security and has received coveted industry certification ISO 9001:2008
                  </p>
                  <p class="text-style-1 offset-top-22">
                  <strong>X.100 Solutions</strong><br>
                  Simplify business technology with the industry's most advanced banking solutions<br><br>

                  <strong>Simplifying Banking</strong><br>
                  The latest version of X.100(V10.5) universal banking solution helps banks simplify banking                  
                </p>
                </div>
              </div>
            </div>
            <div class="cell-xs-12 cell-md-4 cell-lg-3 offset-top-60 offset-md-top-0">
              <div class="inset-md-left-15 inset-md-right-15">
                <div class="range range-40 range-fix widget-list">
                <div class="cell-xs-12 cell-sm-6 cell-md-12 widget-item">
                    <nav class="page-nav">
                      <ul class="page-nav-list">
                        <li class="active"><a href="./">About Us</a></li>
                        <li><a href="./clients/">Our Clients</a></li>
                        <li><a href="./careers/">Careers</a></li>
                        <li><a href="./team/">Our Team</a></li>
                        <li><a href="./alliances/">Alliances</a></li>
                      </ul>
                    </nav>
                  </div>
                  <div class="cell-xs-12 cell-sm-6 cell-md-12 widget-item">
                      <img src="../images/xcore6.jpg" width="100%">
                  </div>
                  <div class="cell-xs-12 cell-sm-6 cell-md-12 widget-item">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Brands carousel-->
      <section class="section-35 section-sm-75 bg-alabaster novi-background bg-cover">
        <div class="shell">
        <div class="cell-xs-12 text-center">
                <h2>Key Partners</h2>
                <hr class="hr-block hr-curious-blue">
              </div>
          <div class="range range-fix">
            <div class="cell-xs-12">
              <div class="owl-carousel owl-carousel-centered" data-items="1" data-xs-items="2" data-sm-items="4" data-md-items="5" data-stage-padding="15" data-margin="30" data-nav="true" data-dots="false">
                <div class="item"><a class="link-image" href="#">
                    <figure><img src="../images/partner_1.png" alt="" width="170" height="75" class="center" style="margin-top: 35%"/>
                    </figure></a></div>
                <div class="item"><a class="link-image" href="#">
                    <figure><img src="../images/partner_2.png" alt="" width="170" height="75" class="center" style="margin-top: 30%"/>
                    </figure></a></div>
                <div class="item"><a class="link-image" href="#">
                    <figure><img src="../images/partner_3.png" alt="" width="170" height="75" class="center"/>
                    </figure></a></div>
                <div class="item"><a class="link-image" href="#">
                    <figure><img src="../images/partner_4.png" alt="" width="170" height="75" class="center" style="margin-top: 25%"/>
                    </figure></a></div>
                <div class="item"><a class="link-image" href="#">
                    <figure><img src="../images/partner_5.png" alt="" width="170" height="75" class="center" style="margin-top: 20%"/>
                    </figure></a></div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Page Footer-->
      <footer class="page-foot">
        <div class="section-45 section-sm-75 bg-river-bed novi-background bg-cover">
          <div class="shell text-center text-sm-left">
            <div class="range range-45 range-xs-center">
              <div class="cell-xs-12 cell-sm-6 cell-lg-3"><a class="brand offset-top-5" href="index.html"><img src="../images/logo-white.png" alt="" width="158" height="40"/></a>
                <p class="offset-sm-top-34">
                  USG is an industry leader in 
                  Financial Software Development and Sales, 
                  Datacenters/Disaster-Recovery Building, 
                  Consulting and Outsourcing.
                </p>
                <div class="elements-group-1 offset-top-22"><span class="small text-italic">Follow Us:</span>
                  <ul class="list-inline">
                    <li><a class="icon icon-xxs icon-circle icon-dark icon-white-filled fa-facebook novi-icon" href="#"></a></li>
                    <li><a class="icon icon-xxs icon-circle icon-dark icon-white-filled fa-twitter novi-icon" href="#"></a></li>
                    <li><a class="icon icon-xxs icon-circle icon-dark icon-white-filled fa-google-plus novi-icon" href="#"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-12 cell-sm-6 cell-lg-3" style="visibility: hidden">
                <h4>Navigation</h4>
                <hr class="hr-variant-1">
                <nav class="footer-nav-wrap text-xs-left">
                  <div class="footer-nav-column">
                    <ul class="footer-nav">
                      <li><a>About</a></li>
                      <li><a>Blog</a></li>
                      <li><a>Contacts</a></li>
                    </ul>
                  </div>
                  <div class="footer-nav-column">
                    <ul class="footer-nav">
                      <li><a>Services</a></li>
                      <li><a>Our team</a></li>
                      <li><a>Blocks</a></li>
                    </ul>
                  </div>
                </nav>
              </div>
              <div class="cell-xs-12 cell-sm-6 cell-lg-3">
                <h4>Contact info</h4>
                <hr class="hr-variant-1">
                <address class="contact-info reveal-inline-block">
                  <ul class="contact-info-list">
                    <li class="unit unit-horizontal unit-spacing-md">
                      <div class="unit-left"><span class="icon icon-sm icon-picton-blue fa-phone novi-icon"></span></div>
                      <div class="unit-body"><span class="text-bold"><a href="tel:#">+233 302 678178<br>+233 26 685 2291</a></span></div>
                    </li>
                    <li class="unit unit-horizontal unit-spacing-md">
                      <div class="unit-left"><span class="icon icon-sm icon-picton-blue fa-envelope-o novi-icon"></span></div>
                      <div class="unit-body"><span><a href="mailto:#">info@unionsg.com</a></span></div>
                    </li>
                    <li class="unit unit-horizontal unit-spacing-md">
                      <div class="unit-left"><span class="icon icon-sm icon-picton-blue fa-clock-o novi-icon"></span></div>
                      <div class="unit-body"><span>Mon - Sat: 8:00 - 18:00</span></div>
                    </li>
                    <li class="unit unit-horizontal unit-spacing-md">
                      <div class="unit-left"><span class="icon icon-sm icon-picton-blue fa-map-marker novi-icon"></span></div>
                      <div class="unit-body"><span><a href="https://www.google.com.gh/maps/place/UNION+SYSTEMS+GLOBAL+GH.LTD/@5.5491945,-0.2356864,17z/data=!3m1!4b1!4m5!3m4!1s0xfdf974f85f3e69f:0x25e5aa81924b253e!8m2!3d5.5491892!4d-0.2334977"><span class="text-bold">No 4, Jungle Lane, </span><span>Lartebiokorshie, Accra, Ghana</span></a></span></div>
                    </li>
                  </ul>
                </address>
              </div>
              <div class="cell-xs-12 cell-sm-6 cell-lg-3">
                <h4>Subscribe</h4>
                <hr class="hr-variant-1">
                <p>Get latest updates and offers.</p>
                <!-- RD Mailform-->
                <form class="rd-mailform rd-mailform-inline max-width-300" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                  <div class="form-group">
                    <label class="form-label" for="footer-contact-email">E-Mail</label>
                    <input class="form-control" id="footer-contact-email" type="email" name="email" data-constraints="@Required @Email">
                  </div>
                  <button class="btn btn-xs btn-picton-blue" type="submit">Send</button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="section-14 bg-ebony-clay novi-background bg-cover">
          <div class="shell text-center">
            <p class="rights">&#169;&nbsp;<span class="copyright-year"></span>&nbsp;Union System Global.<a href="privacy.html">Privacy Policy</a> &bull; Designed by <a href="https://fenixtech.net">Fenix Tech</a></p>
          </div>
        </div>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="../js/core.min.js"></script>
    <script src="../js/script.js"></script>
  </body>
</html>